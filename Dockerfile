FROM node:10-alpine

ENV PORT 3000

# Create app directory
RUN mkdir -p /usr/src/demoApp
WORKDIR /usr/src/demoApp

# Install app dependencies
COPY package*.json /usr/src/demoApp/
RUN npm install

# Bundle app source
COPY . /usr/src/demoApp

RUN npm run build
EXPOSE 3000

CMD [ "npm", "start" ]