import Head from 'next/head';
import Nav from './nav';

const Layout = (props) => (
    <div>
        <Head>
            <title>DemoApp</title>
            <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css"/>
        </Head>
        <Nav/>
        <div className="container">
            {props.children}
        </div>
    </div>
);

export default Layout;